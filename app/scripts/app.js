'use strict';

/**
 * @ngdoc overview
 * @name hungrApp
 * @description
 * # hungrApp
 *
 * Main module of the application.
 */
angular
  .module('hungrApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl'
      })
      .when('/contact', {
        templateUrl: 'views/contact.html',
        controller: 'ContactCtrl'
      })
      .when('/yes-or-no', {
        templateUrl: 'views/yes-or-no.html',
        controller: 'YesornoCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
