'use strict';

/**
 * @ngdoc function
 * @name hungrApp.controller:ContactCtrl
 * @description
 * # ContactCtrl
 * Controller of the hungrApp
 */
angular.module('hungrApp')
  .controller('ContactCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });