'use strict';

/**
 * @ngdoc function
 * @name hungrApp.controller:YesornoCtrl
 * @description
 * # YesornoCtrl
 * Controller of the hungrApp
 */
angular.module('hungrApp')
  .controller('YesornoCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];

    $scope.waitingForPlaces = true;
    $scope.showMap = false;

    $scope.initialise = function initialise() {

    	var myVar;

		function myIntervalFunction() {
		    myVar = setInterval(function(){
		    	if (places) {
		    		
		    		$scope.start(places);
		    		myStopFunction();
		    	}
		    }, 3000);
		}

		function myStopFunction() {
		    clearInterval(myVar);
		}

		myIntervalFunction();

    }

    $scope.start = function start(places){

    $scope.waitingForPlaces = false;
    $scope.$apply();

		var i = 0;

		var end = places.length
		var results = [];

		var plotResults = function plotResults(results){

			for (var i = 0; i < results.length; i++) {  
              $scope.plot(results[i]);
            }
            $scope.showResults();
		  }

    	$('#yes-button').click(function(){
    		if (i<end) {
    			results.push(places[i])
    			i++;
    			showPlace(i);
	    	} else {
	    		plotResults(results)
	    	}
    	});

  		$('#no-button').click(function(){
  			if (i<end) {
      			i++;
      			showPlace(i);
  	    	} else {
  	    		plotResults(results)
  	    	}
      });

    	var showPlace = function showPlace(i) {
    		var place = places[i];
        if (place) {
      		var photoUrl;
          if (place.photos && place.photos[0]) {
          photoUrl = place.photos[0].getUrl({"maxWidth": 600, "maxHeight": 400});
          $("#yes-or-no-image").attr('style', '');
          } else {
            photoUrl= 'assets/img/portfolio/cake.png';
            $("#yes-or-no-image").attr('style', 'width: 600px; height: 400px;');
          }
      		$("#yes-or-no-image").attr('src', photoUrl);
      		$("#yes-or-no-place-name").text(place.name);
        }
    	}

    	showPlace(0);

    }

    $scope.showResults = function showResults() {
  		$scope.showMap = true;
      $scope.$apply();
      $('#results-map').html($('#map-container').show());
    };

    $scope.plot = function plot(place) {
      var image = 'images/kebabpin.png';
      var placeLoc = place.geometry.location;
      var marker = new google.maps.Marker({
        map: map,
        icon: image,
        animation: google.maps.Animation.BOUNCE,
        position: place.geometry.location
      });

      google.maps.event.addListener(marker, 'click', function() {
        infowindow.setContent(place.name);
        infowindow.open(map, this);
      });
    }

    $scope.initialise();
  });
