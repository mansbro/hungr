'use strict';

/**
 * @ngdoc function
 * @name hungrApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the hungrApp
 */
angular.module('hungrApp')
  .controller('AboutCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
