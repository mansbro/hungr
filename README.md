Hungr readme
============

Getting started
---------------


### One time only tasks

Install node.js:
    
    brew install node

Install Compass:

    sudo gem install compass

Install Bower:

    npm install -g bower

### Running the project

In the root directory:

    npm install
    bower install
    grunt serve