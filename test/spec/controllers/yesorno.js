'use strict';

describe('Controller: YesornoCtrl', function () {

  // load the controller's module
  beforeEach(module('hungrApp'));

  var YesornoCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    YesornoCtrl = $controller('YesornoCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
